package com.example.apptecMisantla

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.viewpager.widget.ViewPager
import com.example.apptecMisantla.fragments.LicenciaturasFragment
import com.example.apptecMisantla.fragments.PosgradoFragment
import com.example.apptecMisantla.fragments.SAbiertoFragment
import com.example.apptecMisantla.fragments.adapters.ViewPagerAdapter
import com.firebase.ui.auth.AuthUI
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_oferta.*

class OfertaActivity : AppCompatActivity(){
    var preferences: SharedPreferences? = null
    var sesion: String = "false"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_oferta)
        setSupportActionBar(toolbarMain)
        setFragments()
        preferences = PreferenceManager.getDefaultSharedPreferences(this)
        tvCorreoUsuario.text = intent.getStringExtra("email")
        tvNombreUsuario.text = intent.getStringExtra("nombre")
        sesion = intent.getStringExtra("sesion")

    }

    private fun setFragments(){
        val viewPager = findViewById<ViewPager>(R.id.contenido)
        val tabs = findViewById<TabLayout>(R.id.tabs)

        val adapter = ViewPagerAdapter(supportFragmentManager)

        adapter.addFragment(LicenciaturasFragment(),"LICENCIATURAS")
        adapter.addFragment(SAbiertoFragment(), "SISTEMA ABIERTO")
        adapter.addFragment(PosgradoFragment(), "POSGRADO")


        viewPager.adapter = adapter
        tabs.setupWithViewPager(viewPager)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when(item!!.itemId){
        R.id.menuSalir -> {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            true

        }
        R.id.menuOlvidar -> {
            if(sesion == "true") {
                preferences!!.edit().clear().apply()
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }else{
                AuthUI.getInstance().signOut(this)
                    .addOnCompleteListener{
                        val intent = Intent(this, MainActivity::class.java)
                        startActivity(intent)
                    }
                    .addOnFailureListener {
                            e -> Toast.makeText(this, e.message, Toast.LENGTH_SHORT).show()
                    }
            }
            true
        }
        else -> false
    }

}