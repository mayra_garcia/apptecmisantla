package com.example.apptecMisantla

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.text.TextUtils

import android.util.Patterns
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*

import java.util.*


class MainActivity : AppCompatActivity() {
    private val MY_REQUEST_CODE: Int = 1234
    lateinit var providers: List<AuthUI.IdpConfig>

    var preferences: SharedPreferences? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_MainActivity)
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        //Facebook login
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        providers = Arrays.asList<AuthUI.IdpConfig>(
            AuthUI.IdpConfig.GoogleBuilder().build(),
            AuthUI.IdpConfig.FacebookBuilder().build()
        )

        preferences = PreferenceManager.getDefaultSharedPreferences(this)
        putPreferences()


        btnIngresar.setOnClickListener {
            var user = FirebaseAuth.getInstance().currentUser
            if(txtUsuario.text!!.isNotEmpty() && txtPassword.text!!.isNotEmpty()){
                FirebaseAuth.getInstance().signInWithEmailAndPassword(txtUsuario.text.toString(),txtPassword.text.toString())
                    .addOnCompleteListener {
                        if(it.isSuccessful){
                            val intent = Intent(this, InstructionsActivity::class.java)
                            intent.putExtra("email", txtUsuario.text.toString())
                            intent.putExtra("sesion", "true")
                            intent.putExtra("nombre", "Nombre de usuario")
                            savePreferences(txtUsuario.text.toString(),txtPassword.text.toString())
                            startActivity(intent)
                            finish()
                        }else{
                            showAlert2()
                        }
                    }
            }
        }


        btnIngresarOpciones.setOnClickListener {
            TiposdeLogin()
        }

        tvRegistrar.setOnClickListener {
            val intent = startActivity(Intent(this, CrearCuenta::class.java))
        }
    }

    private fun showAlert2() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Error")
        builder.setMessage("Usuario o contraseña incorrecto")
        builder.setPositiveButton("Aceptar",null)
        val dialog: AlertDialog = builder.create()
        dialog.show()

    }

    private fun savePreferences(email: String, pass: String){
        if(switch1.isChecked){
            preferences!!.edit()
                .putString("email", email)
                .putString("pass", pass)
                .apply()
        }
    }
    private fun putPreferences(){
        val email = preferences!!.getString("email", "")
        val pass = preferences!!.getString("pass", "")
        if(!email.isNullOrEmpty() && !pass.isNullOrEmpty()){
            txtUsuario.setText(email)
            txtPassword.setText(pass)
            switch1.isChecked = true
        }
    }

    private fun TiposdeLogin(){
        startActivityForResult(
            AuthUI.getInstance().createSignInIntentBuilder()
                .setAvailableProviders(providers)
                .setLogo(R.drawable.ic_undraw_elements_cipa) // Set logo drawable
                .build(), MY_REQUEST_CODE
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == MY_REQUEST_CODE){
            val response = IdpResponse.fromResultIntent(data)
            if(resultCode == Activity.RESULT_OK){
                var user = FirebaseAuth.getInstance().currentUser
                Toast.makeText(this, "" + user!!.email, Toast.LENGTH_SHORT).show()
                val intent = Intent(this, InstructionsActivity::class.java)
                intent.putExtra("email", user!!.email)
                intent.putExtra("sesion", "false")
                intent.putExtra("nombre", user.displayName)
                startActivity(intent)
            } else {
                Toast.makeText(this, "" + response!!.error!!.message, Toast.LENGTH_SHORT).show()
            }
        }
    }
}