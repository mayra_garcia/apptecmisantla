package com.example.apptecMisantla.modelo

data class Carrera(val icon:Int, val nombre: String, val modalidad:String, val color:Int, val alumnos:Int,val descripcion:String)