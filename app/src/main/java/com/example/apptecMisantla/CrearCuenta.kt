package com.example.apptecMisantla

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_crear_cuenta.*

class CrearCuenta : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_crear_cuenta)

        tvIngresa.setOnClickListener {
            val intent = startActivity(Intent(this, MainActivity::class.java))
        }

        btnCrearCuenta.setOnClickListener {
            if(tvEmail.text!!.isNotEmpty() && tvContra.text!!.isNotEmpty()){
                FirebaseAuth.getInstance().createUserWithEmailAndPassword(tvEmail.text.toString(),tvContra.text.toString())
                    .addOnCompleteListener {
                        if(it.isSuccessful){
                            Toast.makeText(this,"Usuario creado con éxito",Toast.LENGTH_LONG).show()
                            onBackPressed()
                        }else{
                            showAlert()
                        }
                    }
            }
        }

    }

    private fun showAlert(){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Error")
        builder.setMessage("Este e-mail ya se encuentra en uso")
        builder.setPositiveButton("Aceptar",null)
        val dialog:AlertDialog = builder.create()
        dialog.show()

    }
}