package com.example.apptecMisantla

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.apptecMisantla.base.Carrera
import com.example.apptecMisantla.fragments.adapters.RecyclerAdapter
import kotlinx.android.synthetic.main.activity_carrera.*
import kotlinx.android.synthetic.main.dialog_personalizado.view.*

class CarreraActivity : AppCompatActivity(), RecyclerAdapter.OnOptionClickListener {
    var lista: RecyclerView? = null
    var adaptador:RecyclerAdapter? = null
    var layoutManager: RecyclerView.LayoutManager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_carrera)

        val nombreCarrera = intent.getStringExtra("nombreCarrera")
        val color = intent.getStringExtra("color")

        txtNombreCarrera.text = nombreCarrera.toString()

        val opciones = ArrayList<Carrera>()

        opciones.add(Carrera("Misión",R.drawable.icon_bandera))
        opciones.add(Carrera("Visión", R.drawable.icon_vision))
        opciones.add(Carrera("Objetivo del programa", R.drawable.icon_objetivo))
        opciones.add(Carrera("Campo de acción", R.drawable.icon_herramienta))
        opciones.add(Carrera("Perfiles", R.drawable.icon_user))
        opciones.add(Carrera("Modulo de especialidad", R.drawable.icon_engrane))

        lista = findViewById(R.id.listaOpciones)
        lista?.setHasFixedSize(true)

        layoutManager = GridLayoutManager(this,2)
        lista?.layoutManager = layoutManager

        adaptador = RecyclerAdapter(this,opciones,this)

        lista?.adapter = adaptador
    }


    private fun mostrarDialogoPerzonalizado(nombre: String, icono: Int, informacion:String) {
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)

        val inflater: LayoutInflater = layoutInflater
        val view: View = inflater.inflate(R.layout.dialog_personalizado,null)

        builder.setView(view)

        view.tittleOption.text = nombre
        view.imgOption.setImageResource(icono)
        view.textOption.text = informacion
        val dialog: AlertDialog = builder.create()
        dialog.show()

        view.imgSalir.setOnClickListener{
            dialog.dismiss()
        }
    }

    override fun onOptionClick(nombre: String) {
        val imagen = when(nombre){
            "Misión" ->R.drawable.icon_bandera
            "Visión" ->R.drawable.icon_vision
            "Objetivo del programa" ->R.drawable.icon_objetivo
            "Campo de acción" ->R.drawable.icon_herramienta
            "Perfiles" ->R.drawable.icon_user
            else-> R.drawable.icon_engrane
        }

        mostrarDialogoPerzonalizado(nombre, imagen, inforCarreras(nombre))
    }



    // Información de cada una de las carreras
    private fun inforCarreras(nombre: String): String{
        var informacion = ""
        if(txtNombreCarrera.text=="Ing. Sistemas Computacionales"){
            informacion = when(nombre){
                "Misión" -> "Formar recurso humano integral en el área de ingeniería " +
                        "en sistemas computacionales, con actitud crítica, analítica, creativa e " +
                        "innovadora con valores éticos y humanos que satisfagan las necesidades del " +
                        "entorno social y laboral, contribuyendo en el desarrollo científico y tecnológico."

                "Visión" -> "Ser reconocido como un programa con alto nivel académico comprometida con la investigación," +
                        " desarrollo científico, tecnológico, económico y social encaminada hacia las tendencias de las " +
                        "tecnologías de información y telecomunicaciones de vanguardia mundial para el desarrollo de habilidades " +
                        "y aptitudes que facilite a sus egresados la incursión en el mercado laboral nacional e internacional."

                "Objetivo del programa" ->"Formar profesionistas líderes con visión estratégica y amplio sentido ético; capaz " +
                        "de diseñar, desarrollar, implementar y administrar tecnología computacional para aportar soluciones " +
                        "innovadoras en beneficio de la sociedad; en un contexto global, multidisciplinario y sostenible. "

                "Campo de acción" ->"El Ingeniero en Sistemas Computacionales egresado del ITSM, se desarrolla en empresas o " +
                        "instituciones públicas o privadas, en donde se requiera el desarrollo e innovaciones tecnológicas " +
                        "computacionales, como el desarrollo de sistemas informáticos, aplicaciones en áreas como teléfonos inteligentes, " +
                        "automatización, la inteligencia artificial, aplicaciones multimedia y la computación en nube, así como la configuración " +
                        "y administración de la infraestructura de red computacional que sustenta la competitividad de las empresas."

                "Perfiles" ->"*Perfil de ingreso* \n Habilidad de comunicación oral y escrita.\n" +
                        "Habilidad y responsabilidad para trabajar en equipo.\n" +
                        "Creatividad.\n"+
                        "\n*Perfil de egreso* \nImplementa aplicaciones computacionales para solucionar " +
                        "problemas de diversos contextos, integrando diferentes tecnologías, plataformas o dispositivos."

                else-> "Objetivo de la Especialidad: Formar Profesionales en Ingeniería en Sistemas Computacionales con una visión innovadora " +
                        "y sólida en el desarrollo de aplicaciones orientada a servicios para dispositivos móviles para proponer diferentes " +
                        "alternativas de solución acordes a las necesidades y problemáticas de la región."
            }
        }else if (txtNombreCarrera.text=="Ing. Gestión Empresarial") {
            informacion = when (nombre) {
                "Misión" -> "Formar integralmente profesionistas, con enfoque en competencias en las áreas estratégicas de las organizaciones " +
                        "públicas y privadas, para la toma de decisiones, con actitud de éxito y emprendimiento; alcanzando metas a través de su " +
                        "desempeño ético y profesional, de acuerdo a las necesidades actuales. "

                "Visión" -> "Ser una ingeniería que genere profesionistas capaces de crear, administrar y gestionar empresas, aplicando los " +
                        "conocimientos previamente adquiridos en las áreas estratégicas de las instituciones, para diseñar soluciones preventivas, " +
                        "correctivas y de desarrollo organizacional. "

                "Objetivo del programa" -> "Formar integralmente con enfoque en competencias a los estudiantes de esta carrera, en las áreas clave de " +
                        "empresas pequeñas, medianas y grandes, para la toma de decisiones eficientes y eficaces, con actitud de logro y alto desempeño, " +
                        "en un entorno global. "

                "Campo de acción" -> "El campo de trabajo para los egresados de la carrera de ingeniería en gestión empresarial es amplio, ya que puede prestar " +
                        "sus servicios en cualquier organización productiva de bienes y servicios, tanto del sector privado como del sector público " +
                        "(gobierno federal, estatal y municipal). Los puestos que ocupan son de nivel ejecutivo como gerentes, directores, subdirectores y jefes de " +
                        "departamento. De igual forma, los profesionistas egresados de esta carrera estarán capacitados para generar y emprender proyectos empresariales propios. "

                "Perfiles" -> "*Perfil de ingreso* \nLos aspirantes a ingresar a la carrera deben poseer los siguientes conocimientos y habilidades: \n" +
                        "Trabajar en equipo.\n" + "Ser emprendedor.\n" + "Capacidad de organizar y planificar.\n" + "Comunicación oral y escrita.\n" +
                        "Capacidad de organizar información.\n"+
                        "\n*Perfil de egreso* \nDesarrollar y aplicar habilidades directivas y la ingeniería en el diseño, creación, gestión, desarrollo, fortalecimiento e innovación " +
                        "de las organizaciones, con una orientación sistémica y sustentable para la toma de decisiones en forma efectiva."

                else -> "Objetivo del módulo: Formar profesionales que contribuyen a la adecuada gestión de empresas e innovación de procesos, así como al desarrollo implementación " +
                        "y evaluación de sistemas estratégicos de negocios elevando la eficiencia productiva en un entorno global con ética y responsabilidad social. "

            }
        }else if (txtNombreCarrera.text=="Ing. Civil") {
            informacion = when (nombre) {
                "Misión" -> "Formar profesionales de excelencia, críticos, propositivos capaces de tomar decisiones con liderazgo, conciencia y visión que les permita desempeñarse eficientemente " +
                        "en el ejercicio profesional, que sean reconocidos por su liderazgo para promover el desarrollo sustentable con clara conciencia social."

                "Visión" -> "Ser un programa reconocido y consolidado en la formación de profesionistas en Ingeniería Civil que integra conocimientos científicos, tecnológicos y humanos, " +
                        "constituyendo un referente en el diseño de respuestas ingenieriles en la búsqueda del crecimiento y desarrollo nacional e internacional."

                "Objetivo del programa" -> "Formar profesionistas en ingeniería civil de manera integral, con visión humana, analítica, creativa y emprendedora, capaces de identificar y resolver " +
                        "problemas con eficiencia, eficacia y pertinencia, mediante la planeación, diseño, construcción, operación y conservación de obras de infraestructura, en el marco de la " +
                        "globalización, la sustentabilidad y la calidad, contribuyendo al desarrollo de la sociedad."

                "Campo de acción" -> "El egresado de la carrera de Ingeniería Civil, se puede desempeñar en el sector público o privado desarrollando proyectos como:\n " +
                        "Construcción Vial \nConstrucción Hidro-Sanitaria \nConstrucción Urbana \nConstrucciones Ambientales \nConstrucciones Especiales "

                "Perfiles" -> "*Perfil de egreso* \nCon base en el desempeño esperado para un Ingeniero Civil, es deseable que el aspirante sea poseedor de rasgos que definen su" +
                        " perfil, como lo es: Actitudes de servicio, respeto, preservación del medio ambiente y compromiso institucional para lograr una formación integral y " +
                        "multidisciplinaria de actividades de aprendizaje que le permitan alcanzar los objetivos educacionales para el óptimo ejercicio de su profesión."

                else -> "Objetivo del módulo: Formar profesionales en Ingeniería Civil propositivos con capacidad científica, tecnológica y humana para dar solución a " +
                        "los problemas de desarrollo social, mediante la planeación, diseño, construcción, conservación y operación de obras civiles acorde con la demanda " +
                        "que exige el desarrollo del país; promoviendo el desarrollo sustentable y la calidad de los servicios"

            }
        }else if (txtNombreCarrera.text=="Ing. Electromecánica") {
            informacion = when (nombre) {
                "Misión" -> "La carrera de Ingeniería Electromecánica, surge debido a la necesidad creciente de un mundo cada vez más industrializado; " +
                        "Explotando aéreas del conocimiento tales como la ingeniería eléctrica y la Ingeniería mecánica, permitiendo al egresado desarrollar " +
                        "aptitudes para planear, diseñar, optimizar y controlar una amplia gama de procesos tecnológicos, satisfaciendo la demanda de ingenieros " +
                        "altamente capacitados y contribuyendo de esta forma al avance de la tecnología."

                "Visión" -> "Somos en nuestra zona de influencia, el tecnológico que forma los mejores ingenieros electromecánicos, al contar con laboratorios " +
                        "equipados en la continua actualización de nuestra plantilla docente, dando como resultado que nuestros egresados desempeñen sus funciones " +
                        "de manera eficiente, demostrando en todo momento una actitud responsable, creativa, emprendedora y ética. "

                "Objetivo del programa" -> "Formar profesionistas de excelencia en Ingeniería Electromecánica, con actitud emprendedora, liderazgo y capacidad de: " +
                        "analizar, diagnosticar, diseñar, seleccionar, instalar, administrar, mantener e innovar sistemas electromecánicos, en forma eficiente, segura " +
                        "y económica, considerando las normas y estándares nacionales e internacionales de forma sustentable con plena conciencia ética, humanística y social."

                "Campo de acción" -> "Actividades  asociadas  al mantenimiento,  monitoreo,  instrumentación, automatización  de  sistemas  de  producción, acondicionamiento  de" +
                        "aire,  control  y  mantenimiento  de sistemas térmicos, uso racional de la energía."

                "Perfiles" -> "*Perfiles de ingresado* \nCon base en el desempeño esperado para un Ingeniero Electromecánico, es deseable que el aspirante sea poseedor de " +
                        "rasgos que definen su perfil, como lo es: Actitudes de servicio, respeto, preservación del medio ambiente y compromiso institucional para lograr una " +
                        "formación integral y multidisciplinaria de actividades de aprendizaje que le permitan alcanzar los objetivos educacionales para el óptimo ejercicio de su profesión."

                else -> "Objetivo del módulo: Formar profesionales en Ingeniería Electromecánica con especialidad en Energías Renovables con habilidades para diseñar, " +
                        "implementar y mantener sistemas y dispositivos para el aprovechamiento del potencial energético renovable, como el solar, eólico, geotérmico e " +
                        "hidráulico, para los sectores productivos y de servicios, en apego a las normas nacionales e internacionales; Así como la evaluación y desarrollo " +
                        "de proyectos enfocados en la energía sustentable."

            }
        }else if (txtNombreCarrera.text=="Ing. TIC's") {
            informacion = when (nombre) {
                "Misión" -> "Formar profesionales competentes y competitivas en el campo de las tecnologías de la información y comunicaciones, " +
                        "a nivel nacional e internacional; que contribuyan al desarrollo económico, científico y tecnológico de nuestro país. "

                "Visión" -> "Consolidarse como el mejor programa académico del área de tecnologías de la información de nuestra región; con " +
                        "docentes especializados en el manejo de tecnologías emergentes; con vinculación nacional e internacional, egresando " +
                        "profesionales altamente demandados por instituciones públicas y privadas de nuestro país."

                "Objetivo del programa" -> "Formar profesionistas capaces de integrar y administrar tecnologías de la información y comunicaciones," +
                        "que contribuyan a la productividad y el logro de los objetivos estratégicos de las organizaciones; caracterizándose por ser" +
                        "líderes, críticos, competentes, éticos y con visión empresarial, comprometidos con el desarrollo sustentable."

                "Campo de acción" -> "Como ITIC podrá desarrollarse en empresas o instituciones públicas y privadas tanto a nivel nacional como " +
                        "internacional en donde la innovación tecnológica y la administración de la tecnología sean estratégicas"

                "Perfiles" -> "*Perfil de ingreso* \nCapacidad de adaptarse a los constantes cambios e innovaciones tecnológicas y educativas." +
                        "\n*Perfil de egreso* \nDiseñar, implementar y administrar redes de cómputo y comunicaciones, bajo modelos y estándares " +
                        "internacionales, para satisfacer las necesidades de información de los sistemas sociales, garantizando aspectos de seguridad y calidad."

                else -> "Objetivo del módulo: Formar especialistas capaces de diseñar, programar, administrar e integrar dispositivos inteligentes de " +
                        "comunicaciones, para contribuir al desarrollo científico y tecnológico de la sociedad, de las instituciones y empresas de nuestro país."

            }
        }else if (txtNombreCarrera.text=="Ing. Bioquímica") {
            informacion = when (nombre) {
                "Misión" -> "Formar profesionales, competentes en el campo de la Ingeniería Bioquímica, que contribuyan al desarrollo científico y biotecnológico " +
                        "regional y nacional, aportando a la sociedad los beneficios de la ciencia y la tecnología, actuando siempre con ética, pertinencia y " +
                        "cuidado al medio ambiente."

                "Visión" -> "Ser un programa reconocido y consolidado en la formación de profesionistas en Ingeniería Bioquímica en la región y zona de " +
                        "influencia, que integre los conocimientos científicos, tecnológicos y humanos; brindando servicios y productos de calidad, a los " +
                        "sectores sociales y productivos, comprometidos con el medio ambiente."

                "Objetivo del programa" -> "Formar profesionales íntegros de la Ingeniería Bioquímica competentes para trabajar en equipos multidisciplinarios " +
                        "y multiculturales que, con sentido ético, crítico, creativo, emprendedor y actitud de liderazgo, diseñe, controle, simule y optimice " +
                        "equipos, procesos y tecnologías sustentables que utilicen recursos bióticos y sus derivados, para la producción de bienes y servicios " +
                        "que contribuyan a elevar el nivel de vida de la sociedad."

                "Campo de acción" -> "Algunas de las industrias en las que se pueden desarrollar profesionalmente el (la) Ingeniero(a) Bioquímico(a) son las " +
                        "siguientes: Industria de la Transformación y de Procesos, Laboratorios de Investigación, Industria de Alimentos, Industria farmacéutica, " +
                        "Industria de fermentaciones e Investigación, entre otras.\n"

                "Perfiles" -> "*Perfil de ingreso* \nEl aspirante a ingresar el programa de Ing. Bioquímica debe de cumplir con la evaluación de habilidades " +
                        "matemáticas y habilidades verbales, aplicadas por el Instituto Tecnológico Superior de Misantla." +
                        "\n*Perfil de egreso* \nCon base en el desempeño esperado para un ingeniero bioquímico, es deseable que el aspirante sea poseedor de " +
                        "rasgos que definen su perfil, como lo es: Ejerce su profesión para resolver problemas en su ámbito, trabajando en equipos interdisciplinarios " +
                        "y multiculturales, con liderazgo, sentido crítico, disposición al cambio y comprometido con la calidad."

                else -> "Formar profesionales de Ingeniería Bioquímica que con sentido ético, critico, creativo y emprendedor, diseñen, operen, controlen, simulen " +
                        "y optimicen procesos, que utilicen recursos naturales y sus derivados, a través del desarrollo sostenible, en la producción de bienes y " +
                        "servicios que contribuyan a elevar al nivel de la sociedad."

            }
        }else if (txtNombreCarrera.text=="Ing. Ambiental") {
            informacion = when (nombre) {
                "Misión" -> "Formar Ingenieros Ambientales altamente competitivos, con conocimientos, habilidades, actitudes y valores que los impulsen a desarrollarse " +
                        "profesionalmente en un contexto de globalización en los sectores económicos, productivos y de servicios para cubrir los retos y necesidades que " +
                        "demanda el país; procurando el cuidado del medio ambiente a través del uso de tecnologías limpias para un desarrollo sustentable."

                "Visión" -> "Ser un programa de excelencia y calidad académica con alto prestigio a nivel local y nacional en la formación Ingenieros Ambientales, cuyos " +
                        "logros y liderazgo en los sectores privado, publico y social impulsen el desarrollo sustentable del país; lograr la certificación y acreditación " +
                        "de calidad ante los organismos correspondientes a nivel nacional e internacional."

                "Objetivo del programa" -> "Formar profesionistas en Ingeniería Ambiental éticos, analíticos, críticos y creativos con las competencias para identificar, " +
                        "proponer y resolver problemas ambientales de manera multidisciplinaria, asegurando la protección, conservación y mejoramiento del ambiente bajo un " +
                        "marco legal, buscando el desarrollo sustentable en beneficio de la vida en el planeta."

                "Campo de acción" -> "El ingeniero Ambiental podrá desenvolverse en dependencias del sector público o privado, en empresas de consultoría y asesoría ambiental, " +
                        "como profesionistas independientes, en organizaciones no gubernamentales encaminadas a la promoción de cultura ambiental limpia, industrias químicas, " +
                        "petroquímicas, de energía y de procesos; en empresas manufactureras, del giro de alimentos y bebidas, empresas comercializadoras de equipos ambientales, " +
                        "en centros de investigación e instituciones educativas, entre otras."

                "Perfiles" -> "*Perfil de ingreso* \nLos aspirantes a ingresar a la carrera deben poseer los siguientes conocimientos y habilidades: " +
                        "Interés en el medio ambiente, los recursos naturales y en la búsqueda de soluciones." +
                        "\n*Perfil de egreso* \nEl Ingeniero Ambiental integrará en su desarrollo profesional los conocimientos adquiridos durante su formación en las áreas de " +
                        "matemáticas, física, química aplicadas a las aguas residuales y a la generación de residuos, la ecología, gestión ambiental, seguridad e higiene industrial, " +
                        "la contaminación del aire y los residuos sólidos y peligrosos, así como en auditoría ambiental, diseño de plantas de tratamiento de aguas residuales y rellenos " +
                        "sanitarios, además de la remediación de zonas contaminadas por hidrocarburos para responder a las necesidades de preservación, protección, manejo sustentable y " +
                        "mejoramiento de su entorno. Además de contar con los elementos que le permitan el análisis de su entorno socioeconómico y saber comunicarse en español y en inglés. "

                else -> "Tratamiento y remediación de zonas contaminadas por residuos sólidos y líquidos."

            }
        }else if (txtNombreCarrera.text=="Ing. Petrolera") {
            informacion = when (nombre) {
                "Misión" -> "*No hay información proporcionada por la institución*"

                "Visión" -> "*No hay información proporcionada por la institución*"

                "Objetivo del programa" -> "Forma profesionales con la capacidad para desarrollar la programación, ejecución y " +
                        "la dirección de los procesos de explotación de hidrocarburos, aprovechando de manera sustentable los " +
                        "recursos naturales, atendiendo la preservación del medio ambiente, aplicando para ello las nuevas tecnologías, " +
                        "con habilidades, actitudes, aptitudes analíticas y creativas, de liderazgo y calidad humana, con un espíritu de " +
                        "superación permanente para investigar, desarrollar y aplicar el conocimiento científico y tecnológico."

                "Campo de acción" -> "*No hay información proporcionada por la institución*"

                "Perfiles" -> "*Perfil de egreso* \nIdentifica las características geológicas, petrofísicas y dinámicas que controlan la " +
                        "capacidad de almacenamiento de hidrocarburos y la producción de yacimientos, aplicando tecnología de punta."

                else -> "*No hay información proporcionada por la institución*"

            }
        }else if (txtNombreCarrera.text=="Ing. Industrial") {
            informacion = when (nombre) {
                "Misión" -> "Formar profesionales emprendedores, éticos, creativos, analíticos con sentido crítico, que responden a los retos " +
                        "de la modernización en una economía globalizada, mediante el aprovechamiento óptimo de los recursos disponibles, para " +
                        "el mejoramiento de los sistemas administrativos y de manufactura, que conlleven al incremento de la productividad, en un " +
                        "ambiente interdisciplinario, con respeto a la diversidad cultural y al medio ambiente"

                "Visión" -> "Ser reconocido como un programa sólido en la formación de profesionistas en ingeniería industrial que integra el " +
                        "conocimiento científico, tecnológico, humanista, con preocupación por su entorno; y flexible para mantener una relación " +
                        "con el conocimiento especializado para el desarrollo de habilidades y aptitudes que facilite a sus egresados la incursión " +
                        "en los escenarios nacional e internacional"

                "Objetivo del programa" -> "Formar profesionales, éticos, líderes, creativos y emprendedores en el área de Ingeniería Industrial; " +
                        "competente para diseñar, implantar, administrar, innovar y optimizar sistemas de producción de bienes y servicios; con enfoque " +
                        "sistémico y sustentable en un entorno global."

                "Campo de acción" -> "El Ingeniero Industrial es un profesional que puede incorporarse a instituciones públicas o privadas, tanto a " +
                        "empresas que impliquen tecnologías de punta en este campo como aquellas cuyo nivel tecnológico sea incipiente; así mismo, " +
                        "puede desempeñarse en diversas áreas de aplicación de la Ingeniería Industrial, ya sea en micro, pequeñas, medianas o grandes " +
                        "empresas, como lo pueden ser: Administración, Recursos Humanos, Administración de Tecnología, Producción, Automatización, " +
                        "Comercialización, Investigación y Desarrollo."

                "Perfiles" -> "*Perfil de ingreso* \nCon base en el desempeño esperado para un ingeniero industrial, es deseable que el aspirante sea " +
                        "poseedor de rasgos que definen su perfil, como lo es: Capacidad analítica que le permita comprender y relacionar las variables " +
                        "económicas, administrativas y humanas que intervienen en los sistemas productivos." +
                        "\n*Perfil de egreso* \nDiseña, mejora e integra sistemas productivos de bienes y servicios aplicando tecnologías para su optimización."

                else -> "Objetivo de la Especialidad: Formar profesionales en Ingeniería Industrial con habilidades creativas y directivas, para la generación " +
                        "y administración de empresas industriales, comerciales y/o de servicios que fortalezcan el desarrollo de sus regiones."

            }
        }

        return informacion
    }
}