package com.example.apptecMisantla.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.apptecMisantla.R
import com.example.apptecMisantla.fragments.adapters.RecyclerAdapterOferta
import com.example.apptecMisantla.modelo.Carrera

class PosgradoFragment : Fragment(), RecyclerAdapterOferta.OnCarreraClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val listaPosgrado = listOf(
                Carrera(
                        R.drawable.logomii,
                        "Ingeniería Industrial",
                        "Maestría", R.drawable.ii_color,
                        50,
                        "Dr. David Reyes González\n" +
                                "Coordinador de la Maestría en Ingeniería Industrial\n" +
                                "Email: dreyesg@misantla.tecnm.mx"
                ),
                Carrera(
                        R.drawable.logomsc,
                        "Ing. Sistemas Computacionales",
                        "Maestría", R.drawable.ii_color,
                        50,
                        "Dr. Eddy Sánchez de la Cruz\n" +
                                "Coordinador de la Maestría en Sistemas Computacionales\n" +
                                "Email: esanchezd@misantla.tecnm.mx\n"
                ),
                Carrera(
                        R.drawable.logodci,
                        "Ciencias de la Ingeniería",
                        "Doctorado", R.drawable.ii_color,
                        50,
                        "Dr. Luis Carlos Sandoval Herazo\n" +
                                "Coordinador del Doctorado en Ciencias de la Ingeniería\n" +
                                "Teléfono: (235) 111 5306\n" +
                                "Email:doctorado@misantla.tecnm.mx\n" +
                                "lcsandovalh@misantla.tecnm.mx"
                )
        )
        val view = inflater.inflate(R.layout.fragment_posgrado, container, false)
        val recyclerView = view.findViewById<RecyclerView>(R.id.recyclerViewPosgrado)
        recyclerView.layoutManager = LinearLayoutManager(activity)

        recyclerView.adapter = RecyclerAdapterOferta(activity, listaPosgrado, this)
        // Inflate the layout for this fragment
        return view
    }


    override fun onCarreraClick(nombre: String, color: Int) {

    }

}