package com.example.apptecMisantla.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.apptecMisantla.CarreraActivity
import com.example.apptecMisantla.R
import com.example.apptecMisantla.fragments.adapters.RecyclerAdapterOferta
import com.example.apptecMisantla.modelo.Carrera

class LicenciaturasFragment : Fragment(), RecyclerAdapterOferta.OnCarreraClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val listaCarreras = listOf(
            Carrera(
                R.drawable.logoisc,
                "Ing. Sistemas Computacionales",
                "Sistema Escolarizado",
                R.drawable.isc_color,
                1500,
                "M.C. Guillermo Suárez León\n" +
                        "Jefatura de Ing. en sistemas Computacionales\n" +
                        "Email: gsuarezl@misantla.tecnm.mx"
            ),
            Carrera(
                R.drawable.logoige,
                "Ing. Gestión Empresarial",
                "Sistema Escolarizado",
                R.drawable.ige_color,
                1500,
                "L.C. Maribel García Alvarado\n" +
                        "Jefatura de Ing. en Gestión Empresarial\n" +
                        "Email: mgarciaa@misantla.tecnm.mx"
            ),
            Carrera(
                R.drawable.logoic,
                "Ing. Civil",
                "Sistema Escolarizado",
                R.drawable.ic_color,
                1500,
                "M.C. Zita Montserrat Juárez Reyes\n" +
                        "Jefatura de Ing. Civil\n" +
                        "Email: zmjuarezr@misantla.tecnm.mx"
            ),
            Carrera(
                R.drawable.logoie,
                "Ing. Electromecánica",
                "Sistema Escolarizado",
                R.drawable.ie_color,
                1500,
                "M.C. Saúl Reyes Barajas\n" +
                        "Jefatura de Ing. Electromecánica\n" +
                        "Email: sreyesb@misantla.tecnm.mx"
            ),
            Carrera(
                R.drawable.logotics,
                "Ing. TIC's",
                "Sistema Escolarizado",
                R.drawable.isc_color,
                600,
                "Ing. José Luis Fernández Jiménez\n" +
                        "Jefatura de Ing. TIC's\n" +
                        "Email: jlfernandezj@misantla.tecnm.mx"
            ),
            Carrera(
                R.drawable.logoibq,
                "Ing. Bioquímica",
                "Sistema Escolarizado",
                R.drawable.ibq_color,
                600,
                "Ing. Aracely Romano García\n" +
                        "Jefatura de Ing. Bioquímica\n" +
                        "Email: aromanog@misantla.tecnm.mx"
            ),
            Carrera(
                R.drawable.logoia,
                "Ing. Ambiental",
                "Sistema Escolarizado",
                R.drawable.isc_color,
                300,
                "M.S.C. Elizabeth Salazar Hernández\n" +
                        "Jefatura de Ing. Ambiental\n" +
                        "Email: esalazarh@misantla.tecnm.mx"
            ),
            Carrera(
                R.drawable.logoip,
                "Ing. Petrolera",
                "Sistema Escolarizado",
                R.drawable.ip_color,
                200,
                "Ing. Jesús Uriel Hoyos Villa\n" +
                        "Jefatura de Ing. Petrolera\n" +
                        "Email: juhoyosv@misantla.tecnm.mx"
            ),
            Carrera(
                R.drawable.logoii,
                "Ing. Industrial",
                "Sistema Escolarizado",
                R.drawable.ii_color,
                2500,
                    "M.I.I. Tito Armando Hernández y González\n" +
                            "Jefatura de Ing. Industrial\n" +
                            "Email: tahernandezyg@misantla.tecnm.mx"
            )
        )

        val view = inflater.inflate(R.layout.fragment_licenciaturas, container, false)
        val recyclerView = view.findViewById<RecyclerView>(R.id.recyclerViewLicenciaturas)
        recyclerView.layoutManager = LinearLayoutManager(activity)

        recyclerView.adapter = RecyclerAdapterOferta(activity,listaCarreras,this)

        // Inflate the layout for this fragment
        return view
    }

    override fun onCarreraClick(nombre: String, color: Int) {
        Toast.makeText(activity, "Nombre de la carrera: $nombre",Toast.LENGTH_LONG).show()
        val i = Intent(activity,CarreraActivity::class.java)
        i.putExtra("nombreCarrera",nombre)
        val colorsito = when(nombre){
            "Ing. En Sistemas Computacionales" -> "#4CAF50"
            else -> "1F3D6D"
        }
        i.putExtra("color",colorsito)
        startActivity(i)
    }
}