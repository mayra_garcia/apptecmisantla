package com.example.apptecMisantla.fragments.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.apptecMisantla.R
import com.example.apptecMisantla.base.BaseViewHolder
import com.example.apptecMisantla.modelo.Carrera
import java.lang.IllegalArgumentException

class RecyclerAdapterOferta(val context: FragmentActivity?, val carreras:List<Carrera>, val itemClickListener: OnCarreraClickListener):RecyclerView.Adapter<BaseViewHolder<*>>() {

    interface OnCarreraClickListener{
        fun onCarreraClick(nombre: String, color: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        return LicenciaturasViewHolder(LayoutInflater.from(context).inflate(R.layout.content_item,parent,false))
    }

    override fun getItemCount(): Int = carreras.size

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        when(holder){
            is LicenciaturasViewHolder -> holder.bind(carreras[position],position)
            else -> throw IllegalArgumentException("Sin argumento ViewHolder")
        }
    }

    inner class LicenciaturasViewHolder(itemView: View):BaseViewHolder<Carrera>(itemView){
        override fun bind(item: Carrera, position: Int) {
            itemView.setOnClickListener { itemClickListener.onCarreraClick(item.nombre,item.color) }
            Glide.with(context!!).load(item.icon).into(itemView.findViewById(R.id.iconCarrera))
            itemView.findViewById<TextView>(R.id.nameCarrera).text = item.nombre
            itemView.findViewById<TextView>(R.id.modalidad).text = item.modalidad
            Glide.with(context!!).load(item.color).into(itemView.findViewById(R.id.imgColor))
            itemView.findViewById<TextView>(R.id.countAlumnos).text = "Alumnos inscritos: ${item.alumnos}"
            itemView.findViewById<TextView>(R.id.description).text = item.descripcion
        }
    }


}