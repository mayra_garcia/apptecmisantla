package com.example.apptecMisantla.fragments.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import com.example.apptecMisantla.R
import com.google.android.material.tabs.TabLayout

class SliderAdapter: PagerAdapter {
    var context: Context
    lateinit var layoutInflater: LayoutInflater
    var tabLayout: TabLayout? = null

    constructor(context: Context) {
        this.context = context
    }

    //Arreglos
    val slide_images: ArrayList<Int> = arrayListOf(R.drawable.ic_undraw_education_f8ru,R.drawable.ic_undraw_certification_aif8,R.drawable.ic_undraw_product_iteration_kjok)
    val slides_headings: java.util.ArrayList<String> = arrayListOf("TECNOLÓGICO DE MISANTLA","OFERTA EDUCATIVA","DESCUBRE CARRERAS DE INTÉRES")
    val slides_descs: java.util.ArrayList<String> = arrayListOf(
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam vitae vulputate metus, dictum hendrerit elit. Aliquam ultricies, dui sit amet.",
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam vitae vulputate metus, dictum hendrerit elit. Aliquam ultricies, dui sit amet.",
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam vitae vulputate metus, dictum hendrerit elit. Aliquam ultricies, dui sit amet.")

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object` as RelativeLayout
    }

    override fun getCount(): Int {
        return slides_headings.size
    }


    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view: View = layoutInflater.inflate(R.layout.instruction_layout,container,false)

        var slideImageView: ImageView = view.findViewById(R.id.slide_image) as ImageView
        var slideHeading: TextView = view.findViewById(R.id.slide_heading) as TextView
        var slideDesc: TextView = view.findViewById(R.id.slide_desc) as TextView

        slideImageView.setImageResource(slide_images[position])
        slideHeading.text = slides_headings[position]
        slideDesc.text = slides_descs[position]


        container.addView(view)

        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as RelativeLayout)
    }
}