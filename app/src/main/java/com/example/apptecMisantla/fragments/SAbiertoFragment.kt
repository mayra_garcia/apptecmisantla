package com.example.apptecMisantla.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.apptecMisantla.R
import com.example.apptecMisantla.fragments.adapters.RecyclerAdapterOferta
import com.example.apptecMisantla.modelo.Carrera

class SAbiertoFragment : Fragment(), RecyclerAdapterOferta.OnCarreraClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        val listaSAbierto = listOf(
                Carrera(
                        R.drawable.logoige_sa,
                        "Ing. Gestión Empresarial",
                        "Sistema Abierto",
                        R.drawable.ige_color,
                        500,
                        ""
                ),
                Carrera(
                        R.drawable.logoisc_sa,
                        "Ing. Sistemas Computacionales",
                        "Sistema Abierto",
                        R.drawable.isc_color,
                        500,
                        "M.S.C. Irahan Otoniel José Guzmán\n" +
                                "Coordinación de Ing. en Sistemas Computacionales\n" +
                                "Email: iojoseg@misantla.tecnm.mx"
                ),
                Carrera(
                        R.drawable.logoii_sa,
                        "Ing. Industrial",
                        "Sistema Abierto",
                        R.drawable.ii_color,
                        500,
                        "M.I.I. Tito Armando Hernández y González\n" +
                                "Jefatura de Ing. Industrial\n" +
                                "Email: tahernandezyg@misantla.tecnm.mx")
        )

        val view = inflater.inflate(R.layout.fragment_s_abierto, container, false)
        val recyclerView = view.findViewById<RecyclerView>(R.id.recyclerViewSistemaAbierto)
        recyclerView.layoutManager = LinearLayoutManager(activity)

        recyclerView.adapter = RecyclerAdapterOferta(activity, listaSAbierto, this)

        // Inflate the layout for this fragment

        return view
    }

    override fun onCarreraClick(nombre: String, color: Int) {

    }
}