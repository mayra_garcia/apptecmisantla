package com.example.apptecMisantla.fragments.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.apptecMisantla.R
import com.example.apptecMisantla.base.Carrera

class RecyclerAdapter(var context: Context, items:ArrayList<Carrera>, val item: OnOptionClickListener): RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

    var items:ArrayList<Carrera>? = null
    init {
        this.items = items
    }

    interface OnOptionClickListener{
        fun onOptionClick(nombre: String)
    }



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerAdapter.ViewHolder {
        val vista = LayoutInflater.from(context).inflate(R.layout.carrera_options_layout,parent,false)
        val viewHolder = ViewHolder(vista)
        return viewHolder
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items?.get(position)
        holder.imagen?.setImageResource(item?.imagen!!)
        holder.nombre?.text = item?.nombre
    }

    override fun getItemCount(): Int {
        return items?.count()!!
    }

    inner class ViewHolder(vista: View): RecyclerView.ViewHolder(vista){
        var vista = vista
        var imagen: ImageView? = null
        var nombre: TextView? = null
        init {
            imagen = vista.findViewById(R.id.imgView)
            nombre = vista.findViewById(R.id.txtView)

            vista.setOnClickListener{
                item.onOptionClick(vista.findViewById<TextView>(R.id.txtView).text as String)
            }
        }
    }
}