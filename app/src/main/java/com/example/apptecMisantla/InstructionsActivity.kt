package com.example.apptecMisantla

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import androidx.viewpager.widget.ViewPager
import com.example.apptecMisantla.fragments.adapters.SliderAdapter
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_instructions.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_oferta.*

class InstructionsActivity : AppCompatActivity() {
    var position = 0
    var preferences: SharedPreferences? = null
    var sesion: String = "false"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_instructions)

        val mSlideViewPager: ViewPager = findViewById(R.id.slideViewPager)
        val sliderAdapter = SliderAdapter(this)

        //Obteniendo datos para mandarlos al activity principal
        preferences = PreferenceManager.getDefaultSharedPreferences(this)
        val email = intent.getStringExtra("email")
        val nombre = intent.getStringExtra("nombre")
        sesion = intent.getStringExtra("sesion")

        position = mSlideViewPager!!.currentItem
        next?.setOnClickListener {
            if(position < 3){
                position++
                mSlideViewPager!!.currentItem = position
            }
            if(position == 3){
                val i = Intent(Intent(this, OfertaActivity::class.java))
                i.putExtra("email", email)
                i.putExtra("sesion", sesion)
                i.putExtra("nombre", nombre)
                startActivity(i)
            }

        }

        tabIndicator!!.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                position = tab!!.position
                if(tab.position == 2){
                    next!!.text = "Comenzar"
                }else{
                    next!!.text = "Siguiente"
                }
            }
        })


        tabIndicator?.setupWithViewPager(mSlideViewPager)
        mSlideViewPager.adapter = sliderAdapter
    }
}